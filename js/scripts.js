const bgWrapper = document.getElementById("bg-wrapper");
let i = 1;
setInterval(() => {
  i = i > 6 ? 1 : ++i;
  bgWrapper.style.backgroundImage = `url('./assets/images/bg${i}.jpg')`;
  bgWrapper.animate(
    [
      // keyframes
      { opacity: "0.1" },
      { opacity: "1.0" }
    ],
    {
      // timing options
      duration: 5000,
      iterations: 1,
      easing: "cubic-bezier"
    }
  );
}, 5000);
